<?php
require_once('phprpc_server.php');

function mb_urlencode($url, $charset) {
    $len = mb_strlen($url, $charset);
    $result = "";
    for ($i = 0; $i < $len; $i++) {
        $c = mb_substr($url, $i, 1, $charset);
        if (strlen($c) > 1) $c = urlencode($c);
        $result .= $c;
    }
    return $result;
}

new phprpc_server(array('mb_urlencode', 'mb_convert_encoding'));
?>
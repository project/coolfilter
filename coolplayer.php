<?php
/*
Modules Name: Coolfilter
Modules URI: http://drupal.org/node/61357
Description: display all kinds of media in drupal
Version: 2.9
Author: andot,liukan transplant to drupal
Author URI: http://www.kylinx.net/ & coolcode.cn
*/

//================================================================================
// real player
//================================================================================

if (isset($_GET['coolplayer_src'])) {
    $src = (get_magic_quotes_gpc() ? stripslashes($_GET['coolplayer_src']) : $_GET['coolplayer_src']);
    $width = (get_magic_quotes_gpc() ? stripslashes($_GET['width']) : $_GET['width']);
    $height = (get_magic_quotes_gpc() ? stripslashes($_GET['height']) : $_GET['height']);
    $autoplay = (get_magic_quotes_gpc() ? stripslashes($_GET['autoplay']) : $_GET['autoplay']);
    $loop = (get_magic_quotes_gpc() ? stripslashes($_GET['loop']) : $_GET['loop']);
    $clip = "clip" . rand();
    echo "document.writeln('<object classid=\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\" " .
         "width=\"$width\" height=\"$height\">" .
         "<param name=\"src\" value=\"$src\" />" .
         "<param name=\"controls\" value=\"Imagewindow\" />" .
         "<param name=\"console\" value=\"$clip\" />" .
         "<param name=\"autostart\" value=\"$autoplay\" />" .
         "<param name=\"loop\" value=\"$loop\" />" .
         "<embed src=\"$src\" type=\"audio/x-pn-realaudio-plugin\" " .
         "autostart=\"$autoplay\" loop=\"$loop\" console=\"$clip\" " .
         "width=\"$width\" height=\"$height\" controls=\"Imagewindow\"></embed><br />" .
         "</object><object classid=\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\" " .
         "width=\"$width\" height=\"42\">" .
         "<param name=\"src\" value=\"$src\" />" .
         "<param name=\"controls\" value=\"ControlPanel\" />" .
         "<param name=\"console\" value=\"$clip\" />" .
         "<param name=\"autostart\" value=\"$autoplay\" />" .
         "<param name=\"loop\" value=\"$loop\" />" .
         "<embed src=\"$src\" type=\"audio/x-pn-realaudio-plugin\" " .
         "autostart=\"$autoplay\" loop=\"$loop\" console=\"$clip\" " .
         "width=\"$width\" height=\"42\" controls=\"ControlPanel\"></embed>" .
         "</object>');";
     exit();
}

//================================================================================
// coolplayer_url
//================================================================================
if (isset($_GET['coolplayer_url'])) {
    $url = (get_magic_quotes_gpc() ? stripslashes($_GET['coolplayer_url']) : $_GET['coolplayer_url']);
    echo $url;
    exit();
}

class CoolPlayer {
    var $width;
    var $height;
    var $autoplay;
    var $loop;
    var $charset;
    var $download;
    var $mediatype;
    var $rpcurl;

    var $pluginpath = "modules/coolfilter";
    var $blocks = array();
    
    function CoolPlayer() {
        include("coolplayer_config.php");
        $this->width = $coolplayer_width;
        $this->height = $coolplayer_height;
        $this->autoplay = $coolplayer_autoplay;
        $this->loop = $coolplayer_loop;
        $this->charset = $coolplayer_charset;
        $this->download = $coolplayer_download;
        $this->mediatype = "";
        $this->rpcurl = $coolplayer_rpcurl;

        $this->pluginpath =  $this->pluginpath;
    }
    
    function part_one($content) {
        $content = preg_replace('#\<coolplayer(.*?)\>(.*?)\</coolplayer\>#sie', '$this->do_CoolPlayer($content, \'\\2\', \'\\1\');', $content);
        $content = preg_replace('#\[coolplayer(.*?)\](.*?)\[/coolplayer\]#sie', '$this->do_CoolPlayer($content, \'\\2\', \'\\1\');', $content);
        return $content;    
    }

    function part_two($content) {
        if (count($this->blocks)) {
            $content = str_replace(array_keys($this->blocks), array_values($this->blocks), $content);
            $this->blocks = array();
        }

        return $content;
    }

    function getBlockID($content)
    {
        static $num = 0;

        // Just do a check to make sure the user
        // hasn't (however unlikely) input block replacements
        // as legit text
        do
        {
            ++$num;
            $blockID = "<p>++CoolPlayer_BLOCK_$num++</p>";
        }
        while(strpos($content, $blockID) !== false);

        return $blockID;
    }

    function init_option(&$url, &$options, &$width, &$height, &$autoplay, &$loop, &$charset, &$download, &$mediatype) {

        $url = str_replace(array("\\\"", "\\\'"), array("\"", "\'"), $url);
        $options = str_replace(array("\\\"", "\\\'"), array("\"", "\'"), $options);

        if (preg_match('/width\s*=\s*"(\d*?)"/i', $options, $match) or
            preg_match("/width\s*=\s*'(\d*?)'/i", $options, $match)) {
            $width = (int)$match[1];
        }
        else {
            $width = $this->width;
        }

        if (preg_match('/height\s*=\s*"(\d*?)"/i', $options, $match) or
            preg_match("/height\s*=\s*'(\d*?)'/i", $options, $match)) {
            $height = (int)$match[1];
        }
        else {
            $height = $this->height;
        }

        if (preg_match('/autoplay\s*=\s*"(\w*?)"/i', $options, $match) or
            preg_match("/autoplay\s*=\s*'(\w*?)'/i", $options, $match)) {
            $autoplay = (((strtolower(trim($match[1])) == "on") ||
                        (strtolower(trim($match[1])) == "yes")  ||
                        (strtolower(trim($match[1])) == "true") ||
                        (strtolower(trim($match[1])) == "1"))? "1" : "0");
        } else {
            $autoplay = $this->autoplay;
        }

        if (preg_match('/download\s*=\s*"(\w*?)"/i', $options, $match) or
            preg_match("/download\s*=\s*'(\w*?)'/i", $options, $match)) {
            $download = (((strtolower(trim($match[1])) == "on") ||
                        (strtolower(trim($match[1])) == "yes")  ||
                        (strtolower(trim($match[1])) == "true") ||
                        (strtolower(trim($match[1])) == "show") ||
                        (strtolower(trim($match[1])) == "1"))? "block" : "none");
        }
        else {
            $download = $this->download;
        }

        if (preg_match('/loop\s*=\s*"(\w*?)"/i', $options, $match) or
            preg_match("/loop\s*=\s*'(\w*?)'/i", $options, $match)) {
            $loop = (((strtolower(trim($match[1])) == "on") ||
                    (strtolower(trim($match[1])) == "yes")  ||
                    (strtolower(trim($match[1])) == "true") ||
                    (strtolower(trim($match[1])) == "1"))? "1" : "0");
        } else {
            $loop = $this->loop;
        }

        if (preg_match('/mediatype\s*=\s*"(\w*?)"/i', $options, $match) or
            preg_match("/mediatype\s*=\s*'(\w*?)'/i", $options, $match)) {
            $mediatype = trim($match[1]);
        }
        else {
            $mediatype = $this->mediatype;
        }

        if (preg_match('/charset\s*=\s*"(\.*?)"/i', $options, $match) or
            preg_match("/charset\s*=\s*'(\.*?)'/i", $options, $match)) {
            $charset = trim($match[1]);
        }
        else {
            $charset = $this->charset;
        }

    }


    function do_CoolPlayer($content, $url, $options) {
        $this->init_option($url, $options,
                               $default_width,
                               $default_height,
                               $default_autoplay,
                               $default_loop,
                               $charset,
                               $download,
                               $mediatype);

        $id = rand();
        $url = preg_replace("/<br\s*\/?>/i", "\n", $url);
        $url = trim(strip_tags($url, '<a>'));
        $url = str_replace("\r\n", "\n", $url);
        $url = str_replace("\r", "\n", $url);
        $url = explode("\n", $url);

        $result = "<span class=\"coolplayer_wrapper\">";

        if (count($url) > 1) {
            $result .= "<span class=\"coolplayer_playlist\" id=\"coolplayer_playlist_$id\" style=\"width: " . ($default_width - 2) . "px\">";
            for ($i = 0; $i < count($url); $i++) {
                if (preg_match('/\<a (.*?)\>(.*?)\<\/a\>/i', $url[$i], $match)) {
                    $info = $match[2];
                }
                else {
                    $urlparts = parse_url($url[$i]);
                    if (isset($urlparts['path'])) {
                        $info = basename($urlparts['path']);
                        if ($info == "") $info = $urlparts['path'];
                    }
                    else {
                        $info = $url[$i];
                    }
                }
                $result .= "<a href=\"javascript: coolplayer('" .
                           htmlspecialchars($url[$i]) .
                           "', '$id', '$default_width', '$default_height', " .
                           "'$default_autoplay', '$default_loop', '$charset', '$mediatype');\" title=\"$info\">" . ($i + 1) . "</a> ";
            }
            $result .= "</span>";
        }

        $result .= "<span id=\"coolplayer_container_$id\">";
        $result .= "<script type=\"text/javascript\"><!--\nload_coolplayer('" .
                   addcslashes($url[0], "\0..\037\042\047\134") .
                   "', '$id', '$default_width', '$default_height', " .
                   "'$default_autoplay', '$default_loop', '$charset', '$mediatype');\n//--></script>";
        $result .= "</span>";

        $result .= "<span class=\"coolplayer_info\" id=\"coolplayer_info_$id\" style=\"width: " . ($default_width - 2) . "px;" .
                   "display: $download;\" ondblclick=\"coolplayer_input(this, " .
                   "'$default_width', '$default_height', '$default_autoplay', '$default_loop', '$charset', '$mediatype');\" " .
                   "title=\"" .
                   "Double click to input your media URL, and press enter to play it.".
                   "\">" .
                   "Loading..." .
                   "</span>";

        $result .= "</span>";
        
        $blockID = $this->getBlockID($content);
        $this->blocks[$blockID] = $result;
        return $blockID;
    }

}
?>

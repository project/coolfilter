<?php
/*************************************************************************
* Configure default CoolPlayer options
**************************************************************************/

$coolplayer_width = 480;
$coolplayer_height = 380;
$coolplayer_autoplay = "0";
$coolplayer_loop = "0";
$coolplayer_charset = "GBK";
$coolplayer_download = "block";
$coolplayer_mediatype = "";
$coolplayer_pluginpath = "modules/coolfilter";
$coolplayer_rpcurl = "/drupal/modules/coolfilter/rpc.php";
?>

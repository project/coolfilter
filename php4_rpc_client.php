<?php
    class phprpc_client extends __phprpc_client {
        function __call($function, $arguments, &$return) {
            $return = $this->call($function, $arguments, false);
            return true;
        }
    }
    overload('phprpc_client');
?>
<?php
/*
Modules Name: Coolfilter
Modules URI: http://drupal.org/node/61357
Description: download code files
Version: 0.4
Author: liukan
Author URI: http://www.kylinx.net/
*/
//================================================================================
// Download the code
//================================================================================
if (isset($_GET['download'])) {
    $multisite = $_GET['multi'];
	if($multisite==1)
		chdir('../../../../');
	else
		chdir('../../');
    require_once "includes/bootstrap.inc";
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
    $post = $_GET['p'];
    $download = $_GET['download'];
		 $cool_downloadlink = db_fetch_array(db_query('SELECT body FROM {node_revisions} WHERE nid = %d', $post));
    $content=$cool_downloadlink["body"];
    $search = strtolower($content);
    $pos = 0;
    while (true) {
        $count = 0;
        $pos1 = strpos($search, "<coolcode", $pos);
        $pos2 = strpos($search, "[coolcode", $pos);
        if ($pos1 === false) {
            if ($pos2 === false) {
                exit();
            }
            else {
                $pos = $pos2;
                $bracket = array('[', ']');
            }
        }
        else {
            if ($pos2 === false) {
                $pos = $pos1;
                $bracket = array('<', '>');
            }
            else if ($pos1 < $pos2) {
                $pos = $pos1;
                $bracket = array('<', '>');
            }
            else {
                $pos = $pos2;
                $bracket = array('[', ']');
            }
        }
        $start = $pos++;
        $count = 1;
        while ($count > 0) {
            $pos1 = strpos($search, $bracket[0] . "coolcode", $pos);
            $pos2 = strpos($search, $bracket[0] . "/coolcode" . $bracket[1], $pos);
            if ($pos1 === false) {
                if ($pos2 === false) {
                    exit();
                }
                else {
                    $pos = $pos2;
                    $count--;
                }
            }
            else {
                if ($pos2 === false) {
                    $pos = $pos1;
                    $count++;
                }
                else if ($pos1 < $pos2) {
                    $pos = $pos1;
                    $count++;
                }
                else {
                    $pos = $pos2;
                    $count--;
                }
            }
            $pos++;
        }
        $end = $pos + 10;
        $code = substr($content, $start, $end - $start);
        if (preg_match('#^\<coolcode(.*?)download="' . $download . '"(.*?)\>(.*)\</coolcode\>$#sie', $code, $match) ||
            preg_match('#^\[coolcode(.*?)download="' . $download . '"(.*?)\](.*)\[/coolcode\]$#sie', $code, $match)) {
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"$download\"");
            echo trim($match[3]);
            exit();
        }
    }
}

?>

<?php
require_once('phprpc_server.php');

define('ENABLED_MBSTRING_PHPRPC', true);

if (defined('ENABLED_MBSTRING_PHPRPC') && ENABLED_MBSTRING_PHPRPC) {
    require_once('phprpc_client.php');
    $coolplayer_rpcpath = "http://www.coolcode.cn/wp-content/plugins/coolplayer/mbstring.php";
    $rpc_client = new phprpc_client($coolplayer_rpcpath);
}

if (extension_loaded('mbstring')) {
    function mb_urlencode($url, $charset) {
        $len = mb_strlen($url, $charset);
        $result = "";
        for ($i = 0; $i < $len; $i++) {
            $c = mb_substr($url, $i, 1, $charset);
            if (strlen($c) > 1) $c = urlencode($c);
            $result .= $c;
        }
        return $result;
    }
}
else if (extension_loaded('iconv')) {
    function mb_convert_encoding($str, $to_encoding, $from_encoding) {
        return iconv($from_encoding, $to_encoding, $str);
    }
    if (version_compare(phpversion(), "5", ">=")) {
        function mb_urlencode($url, $charset) {
            $len = iconv_strlen($url, $charset);
            $result = "";
            for ($i = 0; $i < $len; $i++) {
                $c = iconv_substr($url, $i, 1, $charset);
                if (strlen($c) > 1) $c = urlencode($c);
                $result .= $c;
            }
            return $result;
        }
    }
    else if (defined('ENABLED_MBSTRING_PHPRPC') && ENABLED_MBSTRING_PHPRPC) {
        function mb_urlencode($url, $charset) {
            global $rpc_client;
            $result = $rpc_client->mb_urlencode($url, $charset);
            if (get_class($result) == "phprpc_error") {
                return $url;
            }
            else {
                return $result;
            }
        }
    }
    else {
        function mb_urlencode($url, $charset) {
            return $url;
        }
    }
}
else if (defined('ENABLED_MBSTRING_PHPRPC') && ENABLED_MBSTRING_PHPRPC) {
    function mb_urlencode($url, $charset) {
        global $rpc_client;
        $result = $rpc_client->mb_urlencode($url, $charset);
        if (get_class($result) == "phprpc_error") {
            return $url;
        }
        else {
            return $result;
        }
    }
    function mb_convert_encoding($str, $to_encoding, $from_encoding) {
        global $rpc_client;
        $result = $rpc_client->mb_convert_encoding($str, $to_encoding, $from_encoding);
        if (get_class($result) == "phprpc_error") {
            return $str;
        }
        else {
            return $result;
        }
    }
}
else {
    function mb_urlencode($url, $charset) {
        return $url;
    }
    function mb_convert_encoding($str, $to_encoding, $from_encoding) {
        return $str;
    }
}

function fetchURL($url) {
    $url_parsed = parse_url($url);
    $host = $url_parsed["host"];
    if (!isset($url_parsed["port"])) {
        $port = 80;
    }
    else {
        $port = $url_parsed["port"];
    }
    $path = $url_parsed["path"];
    if ($url_parsed["query"] != "") $path .= "?" . $url_parsed["query"];
    $out = "GET $path HTTP/1.1\r\n" .
           "Host: $host\r\n" .
           "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1\r\n" .
           "Connection: close\r\n\r\n";
    $fp = fsockopen(gethostbyname($host), $port, $errno, $errstr, 30);
    fwrite($fp, $out);
    $body = false;
    $in = "";
    while (!feof($fp)) {
        $s = fgets($fp, 1024);
        if ($body) $in .= $s;
        if ($s == "\r\n") $body = true;
    }
    fclose($fp);
    return $in;
}

function to_fullpath($url) {
    $urlparts = parse_url($url);
    $host = "";
    $path = "";
    if (!isset($urlparts['host'])) {
        if (isset($_SERVER["HTTP_HOST"])) {
            $host = $_SERVER["HTTP_HOST"];
        }
        else if (isset($_SERVER["SERVER_NAME"])) {
            $$host = $_SERVER["SERVER_NAME"];
        }
        else {
            $host = "localhost";
        }
        if (!isset($_SERVER["HTTPS"]) ||
            $_SERVER["HTTPS"] == "off"  ||
            $_SERVER["HTTPS"] == "") {
            $host = "http://" . $host;
            if ($_SERVER["SERVER_PORT"] != "80") {
                $host .= ":" . $_SERVER["SERVER_PORT"];
            }
        }
        else {
            $host = "https://" . $host;
            if ($_SERVER["SERVER_PORT"] != "443") {
                $host .= ":" . $_SERVER["SERVER_PORT"];
            }
        }
    }
    if (!isset($urlparts['path'])) {
        $path = "/";
    }
    else if (($urlparts['path']{0} != '/') && ($_SERVER["PHP_SELF"]{0} == '/')) {
        $path = substr($_SERVER["PHP_SELF"], 0, strrpos($_SERVER["PHP_SELF"], '/') + 1);
    }
    $url = $host . $path . $url;
    return $url;
}

function coolplayer_init_url(&$url, &$info, &$type, &$width, &$height, &$autoplay, &$loop, $from_charset, $to_charset, $mediatype) {
    if (preg_match('/\<a (.*?)\>(.*?)\<\/a\>/i', $url, $match)) {
        $info = $match[2];
        $a_option = $match[1];

        if (preg_match('/width\s*=\s*\\"(\d*?)\\"/i', $a_option, $match) or
            preg_match("/width\s*=\s*\\'(\d*?)\\'/i", $a_option, $match)) {
            $width = (int)$match[1];
        }

        if (preg_match('/height\s*=\s*\\"(\d*?)\\"/i', $a_option, $match) or
            preg_match("/height\s*=\s*\\'(\d*?)\\'/i", $a_option, $match)) {
            $height = (int)$match[1];
        }

        if (preg_match('/autoplay\s*=\s*\\"(\w*?)\\"/i', $a_option, $match) or
            preg_match("/autoplay\s*=\s*\\'(\w*?)\\'/i", $a_option, $match)) {
            $autoplay = (((strtolower(trim($match[1])) == "on") ||
                        (strtolower(trim($match[1])) == "yes") ||
                        (strtolower(trim($match[1])) == "true") ||
                        (strtolower(trim($match[1])) == "1"))? "1" : "0");
        }

        if (preg_match('/loop\s*=\s*\\"(\w*?)\\"/i', $a_option, $match) or
            preg_match("/loop\s*=\s*\\'(\w*?)\\'/i", $a_option, $match)) {
            $loop = (((strtolower(trim($match[1])) == "on") ||
                    (strtolower(trim($match[1])) == "yes") ||
                    (strtolower(trim($match[1])) == "true") ||
                    (strtolower(trim($match[1])) == "1"))? "1" : "0");
        }

        if (preg_match('/mediatype\s*=\s*\\"(\w*?)\\"/i', $a_option, $match) or
            preg_match("/mediatype\s*=\s*\\'(\w*?)\\'/i", $a_option, $match)) {
            $mediatype = trim($match[1]);
        }
        if (preg_match('/href\s*=\s*\\"(.*?)\\"/i', $a_option, $match) or
            preg_match("/href\s*=\s*\\'(.*?)\\'/i", $a_option, $match)) {
            $url = $match[1];
        }
        if (preg_match('/charset\s*=\s*\\"(\.*?)\\"/i', $options, $match) or
            preg_match("/charset\s*=\s*\\'(\.*?)\\'/i", $options, $match)) {
            $to_charset = trim($match[1]);
        }
    }
    else {
        $urlparts = parse_url($url);
        if (isset($urlparts['path'])) {
            $info = basename($urlparts['path']);
            if ($info == "") $info = $urlparts['path'];
        }
        else {
            $info = $url;
        }
    }

    $url = to_fullpath($url);

    if ($mediatype != "") {
        $type = trim(strtolower($mediatype));
    }
    else {
        $type = explode(".", $url);
        $type = trim(strtolower($type[count($type) - 1]));
        if (strlen($type) > 4) {
            $type = explode("?", $url);
            $type = explode(".", $type[0]);
            $type = trim(strtolower($type[count($type) - 1])); 
        }
        if (substr($url, 0, 5) == "rtmp:") {
            $type = "rtmp";
        }
        else if (substr($url, 0, 4) == "mms:") {
            $type = "mms";
        }
        else if (substr($url, 0, 5) == "rtsp:") {
            $type = "rtsp";
        }
    }

    if (in_array($type, array('mp3', 'flv', 'rtmp', 'rbs', 'xml', 'rss', 'xspf', 'atom'))) {
        $to_charset = 'utf-8';
    }
    $url = mb_convert_encoding($url, $to_charset, $from_charset);
    $url = mb_urlencode($url, $to_charset);
    $url = str_replace(" ", "%20", $url);

}

function is_stupid_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.stupidvideos.com/player.swf') === 0) ||
        (strpos(strtolower($url), 'http://stupidvideos.com/player.swf') === 0)) {
        if ($info == "player.swf") {
            $info = "Stupid Videos";
        }
        return true;
    }
    return false;
}

function is_evilchili_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.evilchili.com/mediaview') === 0) ||
        (strpos(strtolower($url), 'http://evilchili.com/mediaview') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/\"(http\:\/\/208\.100\.1\.88\/media\_content\/[^(\")]*)\"/i', $content, $match)) {
            $url = $match[1];
            if (preg_match('/<a class=\"main\" href=\"index\"><strong>([^<]*)<\/strong><\/a>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_wildko_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.wildko.com') === 0) ||
        (strpos(strtolower($url), 'http://wildko.com') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/(http\:\/\/www\.wildko\.com\/mediastream\.php\?file=[^\.]*\.flv.WMV)/i', $content, $match)) {
            $url = $match[1];
            if (preg_match('/<h1>([^<]*)<\/h1>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_yikers_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.yikers.com/video_') === 0) ||
        (strpos(strtolower($url), 'http://.yikers.com/video_') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/http\:\/\/www\.yikers\.com\/go\_away\.asx\?id=(\d*)/i', $content, $match)) {
            $url = 'http://www.yikers.com/go_away.asx?id=' . $match[1];
            if (preg_match('/<title>([^<]*)<\/title>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_yikers_game(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.yikers.com/game_') === 0) ||
        (strpos(strtolower($url), 'http://yikers.com/game_') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/http\:\/\/www\.yikers\.com\/games\/([^(\.)]*)\.swf/i', $content, $match)) {
            $url = 'http://www.yikers.com/games/' . $match[1] . ".swf";
            if (preg_match('/<title>([^<]*)<\/title>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_redbalcony_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.redbalcony.com/?vid=') === 0) ||
        (strpos(strtolower($url), 'http://redbalcony.com/?vid=') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/var MoviePath = \'(http\:\/\/media\.redbalcony\.com\/[^(\.)]*\.flv)\'/i', $content, $match)) {
            $url = $match[1];
            if (preg_match('/<title>([^<]*)<\/title>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_redbalcony_game(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.redbalcony.com/?pid=') === 0) ||
        (strpos(strtolower($url), 'http://redbalcony.com/?pid=') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/src=\'(http\:\/\/media\.redbalcony\.com\/[^(\.)]*\.swf)\'/i', $content, $match)) {
            $url = $match[1];
            if (preg_match('/<title>([^<]*)<\/title>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_viddler_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.viddler.com') === 0) ||
        (strpos(strtolower($url), 'http://viddler.com') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/so\.addVariable\(\"flashVarText\"\, \"([^(\")]*)\"/i', $content, $match)) {
            $url = "http://www.viddler.com/flash/publisher.swf?". $match[1];
            if (preg_match('/<h2 id=\"titleEditableHr\">([^<]*)<\/h2>/i', $content, $match)) {
                $info = trim($match[1]);
            }
            return true;
        }
    }
    return false;
}

function is_yoqoo_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.yoqoo.com/v_show/') === 0) ||
        (strpos(strtolower($url), 'http://yoqoo.com/v_show/') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/fo\.addVariable\(\"VideoIDS\"\,([^\)]*)\)\;/i', $content, $match)) {
            $url = "http://www.yoqoo.com/v1.0.0023/v/swf/qplayer.swf?VideoIDS=" . $match[1];
           if (preg_match('/<h3 class=\"f\_14 p\_5\">([^<]*)<\/h3>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    else if ((strpos(strtolower($url), 'http://player.yoqoo.com/player.php/sid/') === 0) ||
        (strpos(strtolower($url), 'http://www.yoqoo.com/v/swf/qplayer.swf') === 0) ||
        (strpos(strtolower($url), 'http://yoqoo.com/v/swf/qplayer.swf') === 0)) {
        if ((strpos($url, $info) !== false) || ($info == "qplayer.swf")) {
            $info = "YoQoo Video";
        }
        return true;
    }
    return false;
}

function is_5show_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.5show.com/swf/5show_player_5.0.swf') === 0) ||
        (strpos(strtolower($url), 'http://5show.com/swf/5show_player_5.0.swf') === 0)) {
        if ($info == "5show_player_5.0.swf") {
            $info = "5show Video";
        }
        return true;
    }
    else if ((strpos(strtolower($url), 'http://www.5show.com/show/show/') === 0) ||
        (strpos(strtolower($url), 'http://5show.com/show/show/') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/headstr4=\"([^\"]*)\"/i', $content, $match)) {
            $url = "http://www.5show.com/swf/5show_player_5.0.swf" . $match[1];
            if (preg_match('/<div class=\"sp\_title\_word\">([^<]*)<\/div>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_6room_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.6rooms.com/player.swf?vid=') === 0) ||
        (strpos(strtolower($url), 'http://6rooms.com/player.swf?vid=') === 0)) {
        if ($info == "player.swf") {
            $info = "6Room Video";
        }
        return true;
    }
    else if ((strpos(strtolower($url), 'http://www.6rooms.com/') === 0) ||
        (strpos(strtolower($url), 'http://6rooms.com/') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/FlashObject\(\"(http\:\/\/www\.6rooms\.com\/player\.swf\?vid=[^(\"|\&)]*)\&/i', $content, $match)) {
            $url = $match[1];
            if (preg_match('/<div id=\"video-watch\">\r\n<h4>([^<]*)<\/h4>/i', $content, $match)) {
                $info = $match[1];
            }
            return true;
        }
    }
    return false;
}

function is_live_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.livevideo.com/video') === 0) ||
        (strpos(strtolower($url), 'http://livevideo.com/video') === 0)) {
        if (preg_match('/video\/([^(\/)]*)\/([^(\/)]*)\/([^(\.)]*)\.aspx/', $url, $match)) {
            $video_id = $match[2];
            $url = "http://www.livevideo.com/flvplayer/flvplayer.swf?video=" .
                   urlencode("http://www.livevideo.com/media/GetFlashVideo.ashx?cid=" . $video_id);
            if (strpos($info, 'aspx') !== false) {
                $info = $match[3];
            }
            return true;
        }
        else if (preg_match('/video\/([^(\/)]*)\/([^(\.)]*)\.aspx/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://www.livevideo.com/flvplayer/flvplayer.swf?video=" .
                   urlencode("http://www.livevideo.com/media/GetFlashVideo.ashx?cid=" . $video_id);
            if (strpos($info, 'aspx') !== false) {
                $info = $match[2];
            }
            return true;
        }
    }
    return false;
}

function is_ifilm_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.ifilm.com') === 0) ||
        (strpos(strtolower($url), 'http://ifilm.com') === 0)) {
        if (preg_match('/ifilm\.com\/video\/([^(\&)]*)/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://www.ifilm.com/efp?flvbaseclip={$video_id}";
            if (strpos($info, $video_id) !== false) {
                $info = "IFilm Video";
            }
            return true;
        }
    }
    return false;
}

function is_atomfilms_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.atomfilms.com:80/a/autoplayer/shareEmbed.swf?keyword=') === 0) ||
        (strpos(strtolower($url), 'http://www.atomfilms.com/a/autoplayer/shareEmbed.swf?keyword=') === 0) ||
        (strpos(strtolower($url), 'http://atomfilms.com/a/autoplayer/shareEmbed.swf?keyword=') === 0)) {
        if ($info == "shareEmbed.swf") {
            if (preg_match('/keyword=([^(\&|$)]*)/', $url, $match)) {
                $info = $match[1];
            }
        }
        return true;
    }
    else if ((strpos(strtolower($url), 'http://www.atomfilms.com/film/') === 0) ||
        (strpos(strtolower($url), 'http://atomfilms.com/film/') === 0)) {
        if (preg_match('/atomfilms\.com\/film\/([^(\.)]*)\.jsp/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://www.atomfilms.com:80/a/autoplayer/shareEmbed.swf?keyword={$video_id}";
            if (strpos($info, $video_id) !== false) {
                $info = $video_id;
            }
            return true;
        }
    }
    return false;
}

function is_gametrailers_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.gametrailers.com/umwatcher.php?id=') === 0) ||
        (strpos(strtolower($url), 'http://gametrailers.com/umwatcher.php?id=') === 0) ||
        (strpos(strtolower($url), 'http://www.gametrailers.com/umwatcher_gate72.swf?umid=') === 0) ||
        (strpos(strtolower($url), 'http://gametrailers.com/umwatcher_gate72.swf?umid=') === 0) ||
        (strpos(strtolower($url), 'http://www.gametrailers.com/remote_wrap.php?umid=') === 0) ||
        (strpos(strtolower($url), 'http://gametrailers.com/remote_wrap.php?umid=') === 0)) {
        if (preg_match('/id=(\d*)/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://www.gametrailers.com/remote_wrap.php?umid={$video_id}";
            if ($info == "umwatcher.php" || $info == "umwatcher_gate72.swf" || $info == "remote_wrap.php") {
                $info = "GameTrailers Video";
            }
            return true;
        }
    }
    else if ((strpos(strtolower($url), 'http://www.gametrailers.com/player.php?') === 0) ||
        (strpos(strtolower($url), 'http://gametrailers.com/player.php?') === 0) ||
        (strpos(strtolower($url), 'http://www.gametrailers.com/remote_wrap.php?mid=') === 0) ||
        (strpos(strtolower($url), 'http://gametrailers.com/remote_wrap.php?mid=') === 0)) {
        if (preg_match('/id=(\d*)/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://www.gametrailers.com/remote_wrap.php?mid={$video_id}";
            if ($info == "player.php" || $info == "remote_wrap.php") {
                $info = "GameTrailers Video";
            }
            return true;
        }
    }
    return false;
}

function is_vsocial_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.vsocial.com') === 0) ||
        (strpos(strtolower($url), 'http://vsocial.com') === 0)) {
        if (preg_match('/v=([^(\&|$)]*)/', $url, $match) || 
            preg_match('/vsocial\.com\/v\/([^(\&|$)]*)/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://static.vsocial.com/flash/ups.swf?d={$video_id}";
            if ((strpos($info, $video_id) !== false) ||
                ($info == 'e.swf') || ($info == 'vp.swf')) {
                $info = "Vsocial Video";
            }
            return true;
        }
        else if (preg_match('/d=([^(\&|$)]*)/', $url, $match)) {
            $docid = $match[1];
            $url = "http://static.vsocial.com/flash/ups.swf?d={$docid}";
            if ((strpos($info, $match[1]) !== false) ||
                ($info == "") || ($info == "index.php")) {
                $info = "Vsocial Video";
            }
            return true;
        }
    }
    return false;
}

function is_youtube_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.youtube') === 0) ||
        (strpos(strtolower($url), 'http://youtube') === 0)) {
        if (preg_match('/v=([^(\&|$)]*)/', $url, $match) || 
            preg_match('/video_id=([^(\&|$)]*)/', $url, $match) ||
            preg_match('/youtube\.com\/v\/([^(\&|$)]*)/', $url, $match)) {
            $video_id = $match[1];
            $url = "http://www.youtube.com/v/$video_id";
            if (strpos($info, $video_id) !== false) {
                $info = "YouTube Video";
            }
            return true;
        }
    }
    return false;
}

function is_tudou_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.tudou.com') === 0) ||
        (strpos(strtolower($url), 'http://tudou.com') === 0)) {
        if (preg_match('/\/programs\/view\/([^(\/|$)]*)/', $url, $match) ||
            preg_match('/\/v\/([^(\/|$)]*)/', $url, $match)) {
            $video_id = $match[1];
            if (strpos($info, $video_id) !== false) {
                $info = "Tudou Video";
            }
            $url = 'http://www.tudou.com/v/' . $video_id;
            return true;
        }
        else if (preg_match('/\/playlist\/id\/([^(\/|$)]*)/', $url, $match) ||
                 preg_match('/\/playlist\/playindex\.do\?lid=([^(\&|$)]*)/', $url, $match)) {
            $playlist_id = $match[1];
            if (strpos($info, $playlist_id) !== false || $info == "playindex.do") {
                $info = "Tudou Playlist Video";
            }
            $url = 'http://www.tudou.com/player/playlist.swf?lid=' . $playlist_id;
            return true;
        }
    }
    return false;
}

function is_odeo_audio(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.odeo.com') === 0) ||
        (strpos(strtolower($url), 'http://odeo.com') === 0)) {
        if (preg_match('/\/audio\/([^(\/|$)]*)/', $url, $match)) {
            $audio_id = $match[1];
            if (strpos($info, $audio_id) !== false || $info == "view") {
                $info = "Odeo Audio";
            }
            $url = 'http://media.odeo.com/flash/odeo_player.swf?v=3&amp;type=audio&amp;id=' . $audio_id;
            return true;
        }
    }
    return false;
}

function is_revver_video(&$url, &$info) {
    if (strpos(strtolower($url), 'http://one.revver.com/watch/') === 0) {
        if (preg_match('/revver\.com\/watch\/([^(\/|$)]*)/', $url, $match)) {
            $video_id = $match[1];
            if (strpos($info, $video_id) !== false || $info == "flv") {
                $info = "Revver Video";
            }
            $url = 'http://flash.revver.com/player/1.0/player.swf?' .
                '&amp;mediaId=' . $video_id . '&amp;affiliateId=0&amp;javascriptContext=true' . '&amp;skinURL=http://flash.revver.com/player/1.0/skins/RevverSkin_1-1.swf' . '&amp;skinImgURL=http://flash.revver.com/player/1.0/skins/night_skin.png' . '&amp;actionBarSkinURL=http://flash.revver.com/player/1.0/skins/RevverActionBarSkin.swf' .
                '&amp;resizeVideo=true';
            return true;
        }
    }
    return false;
}

function is_metacafe_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.metacafe.com/watch/') === 0) ||
        (strpos(strtolower($url), 'http://metacafe.com/watch/') === 0)) {
        if (preg_match('/metacafe\.com\/watch\/([^\/]*)\/([^(\/|$)]*)/', $url, $match)) {
            $video_id = $match[1];
            $video_name = $match[2];
            $url = "http://www.metacafe.com/fplayer/{$video_id}/{$video_name}.swf";
            return true;
        }
    }
    return false;
}

function is_myspace_video(&$url, &$info) {
    if (strpos(strtolower($url), 'http://vids.myspace.com/index.cfm') === 0) {
        if (preg_match('/videoid=([^(\&|$)]*)/i', $url, $match)) {
            $video_id = $match[1];
            if ($info == "index.cfm") {
                $info = "MySpace Video";
            }
            $url = "http://lads.myspace.com/videos/vplayer.swf?m={$video_id}";
            return true;
        }
    }
    return false;
}

function is_mofile_video(&$url, &$info) {
    if (preg_match('/http\:\/\/tv\.mofile\.com\/([A-Za-z\d]*)/i', $url, $match)) {
        $video_id = $match[1];
        if ($info == $video_id) {
            $info = "Mofile Video";
        }
        $url = "http://tv.mofile.com/cn/xplayer.swf?v=" . $video_id;
        return true;
    }
    return false;
}

function is_goear_audio(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.goear.com/listen.php') === 0) ||
        (strpos(strtolower($url), 'http://goear.com/listen.php') === 0)) {
        if (preg_match('/v=([^(\&|$)]*)/i', $url, $match)) {
            $audio_id = $match[1];
            if ($info == "listen.php") {
                $info = "GoEar Audio";
            }
            $url = "http://www.goear.com/files/localplayer.swf?file={$audio_id}";
            return true;
        }
    }
    return false;
}

function is_break_video(&$url, &$info) {
    if (preg_match('/http\:\/\/embed\.break\.com\/([^(\&|$)]*)/', $url, $match)) {
        $video_id = $match[1];
        if ((strpos($info, $video_id) !== false)) {
            $info = "break.com Video";
        }
        return true;
    }
    else if ((strpos(strtolower($url), 'http://www.break.com/index/') === 0) ||
        (strpos(strtolower($url), 'http://break.com/index/') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/(http\:\/\/embed\.break\.com\/[^(\&|$)]*)/', $content, $match)) {
            $url = $match[1];
            if ((strpos($info, '.html') !== false)) {
                $info = substr($info, 0, strlen($info) - 5);
            }
            return true;
        }
    }
    return false;
}

function is_dailymotion_video(&$url, &$info) {
    if (preg_match('/dailymotion\.com\/swf\/([^(\&|$)]*)/', $url, $match)) {
        $video_id = $match[1];
        if ((strpos($info, $video_id) !== false)) {
            $info = "dailymotion Video";
        }
        return true;
    }
    else if ((strpos(strtolower($url), 'http://www.dailymotion.com/') === 0) ||
        (strpos(strtolower($url), 'http://dailymotion.com/') === 0)) {
        $content = fetchURL($url);
        if (preg_match('/dailymotion\.com\/swf\/([^(\&|\"|$)]*)/', $content, $match)) {
            $url = "http://www.dailymotion.com/swf/" . $match[1];
            if (preg_match('/<div class=\"box\_main\_title\"><h1>([^<]*)<\/h1><\/div>/i', $content, $match)) {
                $info = $match[1];
            }
            
        return true;
        }
    }
    return false;
}

function is_brightcove_video(&$url, &$info) {
    if ((strpos(strtolower($url), 'http://www.brightcove.com/title.jsp?') === 0) ||
        (strpos(strtolower($url), 'http://brightcove.com/title.jsp?') === 0)) {
        if (preg_match('/title=([^(\&|$)]*)/', $url, $match)) {
            $url = "http://admin.brightcove.com/destination/player/player.swf?" .
                "allowFullScreen=true&amp;" .
                "initVideoId=" . $match[1] . "&amp;" .
                "servicesURL=http://services.brightcove.com/services&amp;" .
                "viewerSecureGatewayURL=https://services.brightcove.com/services/amfgateway&amp;" .
                "cdnURL=http://admin.brightcove.com";
            if ($info == "title.jsp") {
                $info = "BrightCove Video";
            }
            return true;
        }
    }
    return false;
}

function is_imtv_vlog(&$url, &$info) {
    $mid = "";
    $id = "";
    if (preg_match('/im\.tv\/vlog\/personal\/(\d*)\/(\d*)/', $url, $match)) {
        $mid = $match[1];
        $id = $match[2];
        if ($info == $id) {
            $info = "im.tv vlog";
        }
    }
    if (preg_match('/myvlog\.im\.tv\/\?id=(\d*)\&mid=(\d*)/', $url, $match)) {
        $id = $match[1];
        $mid = $match[2];
        if ($info == "/") {
            $info = "im.tv vlog";
        }
    }
    if (preg_match('/myvlog\.im\.tv\/index\_key2\.swf\?id=(\d*)\&mid=(\d*)/', $url, $match)) {
        $id = $match[1];
        $mid = $match[2];
        if ($info == "index_key2.swf") {
            $info = "im.tv vlog";
        }
    }
    if ($id == "" && $mid == "") {
        return false;
    }
    else {
        $url = "http://myvlog.im.tv/index_key2.swf?id={$id}&amp;mid={$mid}&amp;MemberID=&amp;SessID=&amp;LoginID=&amp;inIMTV=Y&amp;album=0";
        return true;
    }
}

function is_rockyou_video(&$url, &$info) {
    $id = "";
    if (preg_match('/rockyou\.com\/rockyou\.swf\?instanceid=(\d*)/', $url, $match)) {
        $id = $match[1];
        if ($info == "rockyou.swf") {
            $info = "RockYou Video";
        }
    }
    if (preg_match('/rockyou\.com\/show\_my\_gallery\.php\?instanceid=(\d*)/', $url, $match)) {
        $id = $match[1];
        if ($info == "show_my_gallery.php") {
            $info = "RockYou Video";
        }
    }
    if ($id == "") {
        return false;
    }
    else {
        $url = "http://apps.rockyou.com/rockyou.swf?instanceid={$id}";
        return true;
    }
}

function is_myvideo_video(&$url, &$info) {
    if (preg_match('/myvideo\.de\/(watch|movie)\/(\d*)/', $url, $match)) {
        $id = $match[2];
        $url = "http://www.myvideo.de/movie/{$id}";
        if ($info == $id) {
            $info = "MyVideo.de";
        }
        return true;
    }
    return false;
}

function is_clipfish_video(&$url, &$info) {
    if (strpos(strtolower($url), 'http://www.clipfish.de/') === 0) {
        if (preg_match('/videoid=([^(\&|$)]*)/i', $url, $match)) {
            $videoid = $match[1];
            $url = "http://www.clipfish.de/videoplayer.swf?p=0&amp;coop=clipfish&amp;videoid={$videoid}";
            if ($info == "player.php" || $info = "videoplayer.swf") {
                $info = "Clipfish Video";
            }
            return true;
        }
    }
    return false;
}

function is_sevenload_video(&$url, &$info) {
    if (strpos(strtolower($url), 'sevenload') !== false) {
        if (preg_match('/videos\/([^(\/|\-|$)]*)/', $url, $match)) {
            $videoid = $match[1];
            if ((strpos($url, $info) !== false)) {
                $info = "sevenload Video";
            }
            $url = "http://en.sevenload.com/~page/swf/en_GB/player.swf?id={$videoid}" . 
                   "&amp;internal=1&amp;endScreen=1";
            return true;
        }
    }
    return false;
}

function is_google_video(&$url, &$info) {
    if (strpos(strtolower($url), 'http://video.google') === 0) {
        if (preg_match('/docid=([^(\&|$)]*)/i', $url, $match)) {
            $docid = $match[1];
            $url = "http://video.google.com/googleplayer.swf?docId={$docid}";
            if ($info == "videoplay" || $info = "googleplayer.swf") {
                $info = "Google Video";
            }
            return true;
        }
    }
    return false;
}

function coolplayer_init_media(&$url, &$info, &$type,
                               &$src, &$func, &$mime,
                               &$width, &$height, &$autoplay, &$loop) {
                              
    global $coolplayer_default_pluginpath;
    $pluginpath = $coolplayer_default_pluginpath;

//real media
   $func = 'coolplayer_rm';

    if (in_array($type, array('rm', 'rmvb', 'ra', 'rv', 'ram', 'smil', 'smi', 'rtsp'))) {
        $src = $pluginpath . "/coolplayer.php?coolplayer_url=" . htmlentities(urlencode($url));
        $mime = 'audio/x-pn-realaudio-plugin';
        return true;
    }

    if ($type == 'rpm') {
        $src = $url;
        $mime = 'audio/x-pn-realaudio-plugin';
        return true;
    }

//windows media
    $func = 'coolplayer_wm';
 
    if (is_evilchili_video($url, $info)) {
        $type = explode(".", $url);
        $type = trim(strtolower($type[count($type) - 1]));
        if ($type == 'swf') {
            $autoplay = "1";
        }
    }
    
    if (is_wildko_video($url, $info)) {
        $type = 'wmv';
    }

    if (in_array($type, array('asf', 'wm', 'wma', 'wmv', 'wax', 'wvx', 'ogg', 'ape', 'avi', 'mid', 'midi', 'wav', 'mms', 'm3u'))) {
        $src = $url;
        $mime = 'application/x-mplayer2';
        $height += 65;
        return true;
    }

    if ($type == 'asx') {
        $src = $url;
        $mime = 'application/asx';
        $height += 65;
        return true;
    }

    if (is_yikers_video($url, $info)) {
        $src = $url;
        $mime = 'application/x-mplayer2';
        $width = 320;
        $height = 305;
        return true;
    }

//quicktime
    $func = 'coolplayer_qt';

    if (in_array($type, array('mov', 'qt', 'mqv', 'm4v', 'm4a', 'm4b'))) {
        $src = $url;
        $mime = 'video/quicktime';
        return true;
    }

    if (in_array($type, array('mpeg', 'mpg', 'm1s', 'm1v', 'm1a', 'm75', 'm15', 'mp2', 'mpm', 'mpv', 'mpa'))) {
        $src = $url;
        $mime = 'video/x-mpeg';
        return true;
    }

    if (in_array($type, array('flc', 'fli', 'cel'))) {
        $src = $url;
        $mime = 'video/flc';
        return true;
    }

    if (in_array($type, array('aiff', 'aif', 'aifc', 'cdda'))) {
        $src = $url;
        $mime = 'audio/x-aiff';
        return true;
    }

    if (in_array($type, array('rtsp', 'rts'))) {
        $src = $url;
        $mime = 'application/x-rtsp';
        return true;
    }
    
    if (in_array($type, array('3gp', '3gpp'))) {
        $src = $url;
        $mime = 'video/3gpp';
        return true;
    }

    if (in_array($type, array('3g2', '3gp2'))) {
        $src = $url;
        $mime = 'video/3gpp2';
        return true;
    }

    if (in_array($type, array('au', 'snd', 'ulw'))) {
        $src = $url;
        $mime = 'audio/basic';
        return true;
    }

    if (in_array($type, array('smf', 'kar'))) {
        $src = $url;
        $mime = 'audio/x-midi';
        return true;
    }

    if ($type == 'qcp') {
        $src = $url;
        $mime = 'audio/vnd.qcelp';
        return true;
    }

    if ($type == 'sdv') {
        $src = $url;
        $mime = 'video/sd-video';
        return true;
    }

    if ($type == 'bwf') {
        $src = $url;
        $mime = 'audio/x-wav';
        return true;
    }

    if ($type == 'gsm') {
        $src = $url;
        $mime = 'audio/x-gsm';
        return true;
    }

    if ($type == 'amr') {
        $src = $url;
        $mime = 'audio/AMR';
        return true;
    }

    if ($type == 'caf') {
        $src = $url;
        $mime = 'audio/x-caf';
        return true;
    }

    if ($type == 'amc') {
        $src = $url;
        $mime = 'application/x-mpeg';
        return true;
    }

    if ($type == 'mp4') {
        $src = $url;
        $mime = 'video/mp4';
        return true;
    }

    if ($type == 'sdp') {
        $src = $url;
        $mime = 'application/x-sdp';
        return true;
    }

//acrobat pdf
    $func = 'coolplayer_pdf';

    if ($type == 'pdf') {
        $src = $url;
        $mime = 'application/pdf';
        return true;
    }

    if ($type == 'fdf') {
        $src = $url;
        $mime = 'application/vnd.fdf';
        return true;
    }

    if ($type == 'xfdf') {
        $src = $url;
        $mime = 'application/vnd.adobe.xfdf';
        return true;
    }

    if ($type == 'xdp') {
        $src = $url;
        $mime = 'application/vnd.adobe.xdp+xml';
        return true;
    }

    if ($type == 'xfd') {
        $src = $url;
        $mime = 'application/vnd.adobe.xfd+xml';
        return true;
    }

//images
    $func = 'coolplayer_img';
    
    if (in_array($type, array('gif', 'jpg', 'jpeg', 'bmp', 'png', 'xpm'))) {
        $src = $url;
        $mime = 'image';
        return true;
    }

// director
    $func = 'coolplayer_dcr';
    if (in_array($type, array('dir', 'dxr', 'dcr', 'cst', 'cct', 'cxt', 'w3d', 'fgd', 'swa'))) {
        $src = $url;
        $mime = 'application/x-director';
        return true;
    }
    
// flash
    $func = 'coolplayer_flash';
    $mime = 'application/x-shockwave-flash';

    if (is_yikers_game($url, $info)) {
        $type = 'swf';
        $autoplay = '1';
    }

    if (is_redbalcony_game($url, $info)) {
        $type = 'swf';
        $autoplay = '1';
    }

    if (is_dailymotion_video($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_atomfilms_video($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_brightcove_video($url, $info)) {
        $src = $url . "&amp;autoStart=" . (($autoplay == 1) ? "true" : "false");
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_stupid_video($url, $info)) {
        $src = $url;
        $width = 450;
        $height = 370;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_ifilm_video($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_gametrailers_video($url, $info)) {
        $src = $url;
        $height = round($width * 0.85);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_live_video($url, $info)) {
        $src = $url;
        $height = round($width * 0.91);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_vsocial_video($url, $info)) {
        $src = $url;
        $height = round($width * 0.975);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_youtube_video($url, $info)) {
        $src = $url;
        $height += 20;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_viddler_video($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_tudou_video($url, $info)) {
        $src = $url;
        $width = 490;
        $height = 423;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_yoqoo_video($url, $info)) {
        $src = $url . "&amp;isAutoPlay=" . (($autoplay == 1) ? "true" : "false");
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_5show_video($url, $info)) {
        $src = $url;
        $height = round($width * 0.93);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_6room_video($url, $info)) {
        $src = $url . "&amp;flag=" . $autoplay;
        $height = round($width * 0.84);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_mofile_video($url, $info)) {
        $src = $url . "&amp;autoplay=" . $autoplay;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_imtv_vlog($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_odeo_audio($url, $info)) {
        $src = $url . "&amp;auto_play=" . (($autoplay == 1) ? "true" : "false");
        $height = round($width * 0.14);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_revver_video($url, $info)) {
        $src = $url . "&amp;width=" . $width . "&amp;height=" . $height;
        $height += 30;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_metacafe_video($url, $info)) {
        $src = $url;
        $height += 50;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_myspace_video($url, $info)) {
        $src = $url . "&amp;a=" . $autoplay;
        $height += 25;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_rockyou_video($url, $info)) {
        $src = $url . "&amp;appWidth={$width}&amp;appHeight={$height}";
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_goear_audio($url, $info)) {
        $src = $url;
        $height = round($width * 0.2);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_break_video($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_myvideo_video($url, $info)) {
        $src = $url;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_clipfish_video($url, $info)) {
        $src = $url;
        $height = round($width * 0.82);
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_sevenload_video($url, $info)) {
        $src = $url . "&amp;autostart=" . $autoplay;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_google_video($url, $info)) {
        $src = $url;
        $height += 27;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if ($type == 'swf') {
        $src = $url;
        $autoplay = (($autoplay == '1') ? 'true' : 'false');
        $loop = (($loop == '1') ? 'true' : 'false');
        return true;
    }

    if ($type == 'spl') {
        $src = $url;
        $mime = 'application/futuresplash';
        $autoplay = (($autoplay == '1') ? 'true' : 'false');
        $loop = (($loop == '1') ? 'true' : 'false');
        return true;
    }

    if ($type == 'mp3') {
        $autoplay = (($autoplay == '1') ? 'true' : 'false');
        $loop = (($loop == '1') ? 'true' : 'false');
        $src = $pluginpath .
               '/mp3player.swf?file=' .
               htmlentities(urlencode($url)) .
               "&amp;autostart=$autoplay&amp;repeat=$loop&amp;width=$width&amp;height=$height&amp;showeq=true";
        $height = 90;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

    if (is_redbalcony_video($url, $info)) {
        $type = 'flv';
    }

    if (in_array($type, array('flv', 'rtmp', 'rbs', 'xml', 'rss', 'xspf', 'atom'))) {
        $autoplay = (($autoplay == '1') ? 'true' : 'false');
        $loop = (($loop == '1') ? 'true' : 'false');
        $src = $pluginpath .
               '/mediaplayer.swf?file=' .
               htmlentities(urlencode($url)) .
               "&amp;autostart=$autoplay&amp;repeat=$loop&amp;width=$width&amp;height=$height";
        $height += 20;
        $autoplay = 'true';
        $loop = 'false';
        return true;
    }

//unknown
    $func = 'coolplayer_unknown';
    $mime = '';
    return false;
}

$coolplayer_default_pluginpath = to_fullpath(substr($_SERVER["PHP_SELF"], 0, strrpos($_SERVER["PHP_SELF"], '/')));

function play_media($url, $id, $width, $height, $autoplay, $loop, $charset, $mediatype="") {
    $url = trim(strip_tags($url, '<a>'));
    coolplayer_init_url($url, $info, $type, $width, $height, $autoplay, $loop, 'utf-8', $charset, $mediatype);
    coolplayer_init_media($url, $info, $type, $src, $func, $mime, $width, $height, $autoplay, $loop);
    return array('url' => $url,
                 'info' => $info,
                 'id' => $id,
                 'src' => $src,
                 'func' => $func,
                 'mime' => $mime,
                 'width' => $width,
                 'height' => $height,
                 'autoplay' => $autoplay,
                 'loop' => $loop);
}

function coolplayer_rpc_version() {
    return "9.012";
}

new phprpc_server(array('play_media', 'coolplayer_rpc_version'));
?>